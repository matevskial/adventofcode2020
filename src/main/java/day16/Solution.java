package day16;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

import util.InputUtils;

public class Solution {
  static Fields fields;
  static List<Integer> myTicket;
  static List<List<Integer>> tickets;
  static Set<Integer> invalidTickets;

  public static void parseInput(List<String> lines) {
    fields = new Fields();
    myTicket = new ArrayList<>();
    tickets = new ArrayList<>();
    int i = 0;
    for(; i < lines.size(); i++) {

      if(lines.get(i).isBlank()){
        break;
      }

      String[] parts = lines.get(i).split(":\\s+");
      String fieldName = parts[0];

      String[] ranges = parts[1].split("\\s+or\\s+");
      for(String r : ranges) {
        int left = Integer.parseInt(r.substring(0, r.indexOf('-')));
        int right = Integer.parseInt(r.substring(r.indexOf('-') + 1));
        fields.putField(fieldName, left, right);
      }
    }

    for(i = i + 2; i < lines.size(); i++) {
      if(lines.get(i).isBlank())
        break;

      myTicket = Arrays.stream(lines.get(i).split(","))
                       .map(Integer::parseInt).collect(Collectors.toList());
    }

    for(i = i + 2; i <lines.size(); i++) {
      List<Integer> ticket = Arrays.stream(lines.get(i).split(","))
                                   .map(Integer::parseInt).collect(Collectors.toList());
      tickets.add(ticket);
    }
  }

  public static int solvePart1() {
    int sum = 0;
    invalidTickets = new HashSet<>();
    for(int i = 0; i < tickets.size(); i++) {
      List<Integer> ticket = tickets.get(i);
      boolean invalidTicket = false;
      for(Integer f : ticket) {
        if(!fields.isValidForAll(f)) {
          sum += f;
          invalidTicket = true;
        }
      }
      if(invalidTicket)
        invalidTickets.add(i);
    }

    return sum;
  }

  public static boolean isInRange(String key, int f) {
    return fields.isValidForField(key, f);
  }

  public static List<List<String>> getPotentialFields() {
    int countFields = myTicket.size();
    List<List<String>> result = new ArrayList<>();
    for(int i = 0; i < countFields; i++) {
      List<Integer> fieldValues = new ArrayList<>();
      for(int r = 0; r < tickets.size(); r++) {
        if(invalidTickets.contains(r))
          continue;
        fieldValues.add(tickets.get(r).get(i));
      }

      result.add(new ArrayList<>());
      for(String key : fields.fields.keySet()) {
        boolean potential = true;
        for(Integer f : fieldValues) {
          potential &= isInRange(key, f);
        }
        if(potential)
          result.get(result.size() - 1).add(key);
      }
    }

    return result;
  }

  public static void addPositions(Set<Integer> processedPositions,
                                  Queue<Integer> positionsToProcess,
                                  List<List<String>> positionFields) {
    for(int i = 0; i < positionFields.size(); i++) {
      if(positionFields.get(i).size() == 1 && !processedPositions.contains(i)) {
        processedPositions.add(i);
        positionsToProcess.offer(i);
      }
    }
  }

  public static void determinePositionFields(List<List<String>> positionFields) {
    Set<Integer> processedPositions = new HashSet<>();
    Queue<Integer> positionsToProcess = new LinkedList<>();
    addPositions(processedPositions, positionsToProcess, positionFields);

    while(!positionsToProcess.isEmpty()) {
      int currPosition = positionsToProcess.poll();
      String field = positionFields.get(currPosition).get(0);

      for(int i = 0; i < positionFields.size(); i++) {
        if(i == currPosition)
          continue;

        positionFields.get(i).remove(field);
      }

      addPositions(processedPositions, positionsToProcess, positionFields);
    }

  }

  public static long solvePart2() {
    List<List<String>> positionFields = getPotentialFields();
    determinePositionFields(positionFields);

    for(int i = 0; i < positionFields.size(); i++) {
      System.out.print("pos: " + i+ " ");
      for (String p : positionFields.get(i)) {
        System.out.print(p + "; ");
      }
      System.out.println();
    }

    long result = 1;
    for(int i = 0; i < myTicket.size(); i++) {
      if(positionFields.get(i).get(0).startsWith("departure"))
        result *= myTicket.get(i);
    }

    return result;
  }

  public static void main(String[] args) {
    List<String> lines = InputUtils.readLines("day16/input.txt");
    parseInput(lines);

    int part1 = solvePart1();
    System.out.println("part1: " + part1);

    long part2 = solvePart2();
    System.out.println("part2: " + part2);
  }
}

class Fields {
  Map<String, List<Range>> fields;

  public Fields() {
    fields = new HashMap<>();
  }

  public void putField(String fieldName, int leftRange, int rightRange) {
    List<Range> l = fields.getOrDefault(fieldName, new ArrayList<>());
    l.add(new Range(leftRange, rightRange));
    fields.put(fieldName, l);
  }

  public boolean isValidForAll(int f) {
    for(String key : fields.keySet()) {
      List<Range> ranges = fields.get(key);
      boolean isInRange = false;

      for(Range r : ranges) {
        if(f >= r.left && f <= r.right) {
          isInRange = true;
          break;
        }
      }

      if(isInRange)
        return true;
    }
    return false;
  }

  public boolean isValidForField(String field, int f) {
    boolean invalid = true;
    for(Range r : fields.getOrDefault(field, new ArrayList<>())) {
      if(f >= r.left && f <= r.right) {
        invalid = false;
        break;
      }
    }

    return !invalid;
  }
}

class Range {
  public int left, right;

  public Range(int left, int right) {
    this.left = left;
    this.right = right;
  }
}