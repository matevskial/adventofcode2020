package day18;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import util.InputUtils;
import util.PuzzlePart;

public class Solution {

  private static Map<String, Integer> priorities;

  public static List<String> tokenize(String expression) {
    String[] parts = expression.split("\\s+");
    List<String> result = new ArrayList<>();
    for(String p : parts) {
      int countOpeningParen = 0, countClosingParen = 0;
      int indx = 0;
      while(indx < p.length() && p.charAt(indx) == '(')  {
        indx++;
        countOpeningParen++;
      }

      int indx1 = p.length() - 1;
      while(indx1 >= 0 && p.charAt(indx1) == ')')  {
        indx1--;
        countClosingParen++;
      }

      for(int c = 0; c < countOpeningParen; c++) {
        result.add("(");
      }

      result.add(p.substring(indx, indx1 + 1));

      for(int c = 0; c < countClosingParen; c++) {
        result.add(")");
      }
    }

    return result;
  }

  public static boolean biggerPriority(String o1, String o2) {
    return priorities.get(o1) >= priorities.get(o2);
  }

  public static List<String> convertToPostfixNotation(List<String> tokens) {
    List<String> result = new ArrayList<>();
    Stack<String> operators = new Stack<>();

    for (String t : tokens) {
      if (t.equals("(")) {
        operators.push(t);
      } else if (t.equals(")")) {
        while (!operators.isEmpty() && !operators.peek().equals("(")) {
          result.add(operators.pop());
        }
        if (!operators.isEmpty() && operators.peek().equals("("))
          operators.pop();
      } else if (t.equals("+") || t.equals("*")) {
        while (!operators.isEmpty() && !operators.peek().equals("(") && biggerPriority(operators.peek(), t)) {
          result.add(operators.pop());
        }
        operators.push(t);
      } else {
        result.add(t);
      }
    }
    while(!operators.isEmpty() && !operators.peek().equals(")")) {
      result.add(operators.pop());
    }

    return result;
  }

  public static Long operation(String op, Long op1, Long op2) {
    if(op.equals("+")) {
      return op1 + op2;
    } else if(op.equals("*")) {
      return op1 * op2;
    } else {
      return -1L;
    }
  }

  public static long evaluatePostfixExpression(List<String> postFix) {
    Stack<Long> st = new Stack<>();

    for(String t : postFix) {
      if(t.equals("+") || t.equals("*")) {
        Long op1 = st.pop();
        Long op2 = st.pop();
        st.push(operation(t, op1, op2));
      } else {
        st.push(Long.parseLong(t));
      }
    }

    return st.pop();
  }

  public static long solve(List<String> expressions, PuzzlePart part) {
    if(part == PuzzlePart.PART_ONE)
      priorities = Map.of("+", 1, "*", 1);
    else
      priorities = Map.of("+", 2, "*", 1);

    long result = 0;
    for(String expression : expressions) {
      List<String> postFix = convertToPostfixNotation(tokenize(expression));
      result += evaluatePostfixExpression(postFix);
    }

    return result;
  }

  public static void main(String[] args) {
    List<String> expressions = InputUtils.readLines("day18/input.txt");

    long part1 = solve(expressions, PuzzlePart.PART_ONE);
    System.out.println("part1: " + part1);

    long part2 = solve(expressions, PuzzlePart.PART_TWO);
    System.out.println("part2: " + part2);
  }
}
