package day15;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import util.InputUtils;

public class Solution {

  public static class Turn {
    int turn;
    int current;
    int previous;

    public Turn(int turn, int current, int previous) {
      this.turn = turn;
      this.current = current;
      this.previous = previous;
    }
  }

  public static Turn putTurn(Map<Integer, Turn> turns, int entry, int turn) {
    Turn prevTurn = turns.getOrDefault(entry, new Turn(entry, turn, turn));
    prevTurn.previous = prevTurn.current;
    prevTurn.current = turn;
    turns.put(entry, prevTurn);
    return prevTurn;
  }

  public static int solve(List<Integer> numbers, int times) {
    Map<Integer, Turn> turns = new HashMap<>();
    int turn = 0;
    for(; turn < numbers.size(); turn++) {
      int n = numbers.get(turn);
      turns.put(n, new Turn(n, turn, turn));
    }

    Turn last = turns.get(numbers.get(numbers.size() - 1));
    for(; turn < times; turn++) {
      int diff = last.current - last.previous;
      last = putTurn(turns, diff, turn);
    }

    return last.turn;
  }

  public static void main(String[] args) {
    List<Integer> numbers = InputUtils.readAsListOfIntegers("day15/input.txt");

    int part1 = solve(numbers, 2020);
    System.out.println("part 1: " + part1);

    int part2 = solve(numbers, 30000000);
    System.out.println("part 2: " + part2);
  }
}