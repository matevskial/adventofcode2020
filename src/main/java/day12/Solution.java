package day12;

import java.util.List;
import java.util.Map;

import util.InputUtils;

public class Solution {

  private static Map<Character, Integer> posMap = Map.of('N', 0, 'E', 1,
                                         'S', 2, 'W', 3);

  public static int solvePart1(List<String> instructions) {
    int[] position = new int[4];
    // north - 0, east - 1, south - 2, west - 3
    int dir = 1;
    for(String instr : instructions) {
      char i = instr.charAt(0);
      int amount = Integer.parseInt(instr.substring(1));

      if(i == 'F')
        position[dir] += amount;
      else if(i == 'R')
        dir = (dir + amount / 90) % 4;
      else if(i == 'L')
        dir = (dir - amount / 90 + 4) % 4;
      else
        position[posMap.get(i)] += amount;
    }

    return Math.abs(position[0] - position[2]) + Math.abs(position[1] - position[3]);
  }

  private static void rotateRight(int[] arr) {
    int put = arr[0];
    arr[0] = arr[arr.length - 1];
    for(int i = 1; i < arr.length; i++) {
      int tmp = arr[i];
      arr[i] = put;
      put = tmp;
    }
  }

  private static void rotateLeft(int[] arr) {
    int put = arr[arr.length - 1];
    arr[arr.length - 1] = arr[0];
    for(int i = arr.length - 2; i >= 0; i--) {
      int tmp = arr[i];
      arr[i] = put;
      put = tmp;
    }
  }

  public static int solvePart2(List<String> instructions) {
    int[] position = new int[4];
    int[] waypoint = new int[] {1, 10, 0, 0};

    for(String instr : instructions) {
      char i = instr.charAt(0);
      int amount = Integer.parseInt(instr.substring(1));

      if(i == 'F') {
        for(int p = 0; p < 4; p++) {
          position[p] += (amount * waypoint[p]);
        }
      } else if(i == 'R') {
        int times = amount / 90;
        for(int c = 0; c < times; c++)
          rotateRight(waypoint);
      }else if(i == 'L') {
        int times = amount / 90;
        for(int c = 0; c < times; c++)
          rotateLeft(waypoint);
      } else {
        waypoint[posMap.get(i)] += amount;
      }
    }

    return Math.abs(position[0] - position[2]) + Math.abs(position[1] - position[3]);
  }

  public static void main(String[] args) {
    List<String> instructions = InputUtils.readLines("day12/input.txt");

    int part1 = solvePart1(instructions);
    System.out.println("part1: " + part1);

    int part2 = solvePart2(instructions);
    System.out.println("part2: " + part2);
  }
}