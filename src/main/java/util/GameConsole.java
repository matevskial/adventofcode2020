package util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GameConsole {
  private List<String> program;
  private long acc;

  int pc;
  Set<Integer> seenInstructions = new HashSet<>();
  public GameConsole(List<String> program) {
    this.program = new ArrayList<>(program);
  }

  public boolean execute(boolean pauseOnLoop) {
    while(pc < program.size()) {
      if(pauseOnLoop && seenInstructions.contains(pc))
        return true;

      if(pauseOnLoop)
        seenInstructions.add(pc);

      List instr = parseInstruction(program.get(pc));

      if(instr.get(0).equals("nop")){
        pc++;
        continue;
      } else if(instr.get(0).equals("acc")) {
        pc++;
        acc += (Long)instr.get(1);
      } else if(instr.get(0).equals("jmp")){
        pc += (Long)instr.get(1);
      }
    }
    return false;
  }

  public long getAcc() {
    return acc;
  }

  private List parseInstruction(String instruction){
    String[] parts = instruction.split("\\s+");
    return Arrays.asList(parts[0], Long.parseLong(parts[1]));
  }
}
