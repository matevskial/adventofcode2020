package util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class InputUtils {
  public static List<String> readLines(String filename) {
    ClassLoader classLoader = InputUtils.class.getClassLoader();
    File file = new File(Objects.requireNonNull(classLoader.getResource(filename)).getFile());

    try {
      return Files.readAllLines(file.toPath());
    } catch (IOException e) {
      String msg = String.format("Error reading input file: %s", filename);
      throw new RuntimeException(msg);
    }
  }

  public static List<Integer> readAsListOfIntegers(String filename) {
    List<String> lines = readLines(filename);
    List<Integer> result = new ArrayList<>();
    for(String l : lines) {
      for(String p : l.split(","))
        result.add(Integer.parseInt(p));
    }

    return result;
  }

  /**
   * This method converts List of strings to List of Lists of characters
   * Useful for algorithms that modify a 2D map represented by the input
   * @param filename
   * @return
   */
  public static char[][] readLinesAs2DMap(String filename) {
    List<String> lines = readLines(filename);
    int rows = lines.size();
    int cols = rows > 0 ? lines.get(0).length() : 0;
    char[][] map = new char[rows][cols];

    for(int i = 0; i < lines.size(); i++) {
      for(int j = 0; j < lines.get(i).length(); j++) {
        map[i][j] = lines.get(i).charAt(j);
      }
    }

    return map;
  }

  public static List<Character> stringAsListOfChars(String s) {
    return s.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
  }
}
