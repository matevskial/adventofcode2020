package day11;

import java.util.Arrays;

import util.InputUtils;
import util.PuzzlePart;

public class Solution {

  private static boolean equalMaps(char[][] m1, char[][] m2) {
    for(int i = 0; i < m1.length; i++) {
      for(int j = 0; j < m1[0].length; j++) {
        if(m1[i][j] != m2[i][j])
          return false;
      }
    }

    return true;
  }

  private static void printMap(char[][] map) {
    for(int i = 0; i < map.length; i++) {
      for(int j = 0; j < map[0].length; j++) {
        System.out.print(map[i][j] + " ");
      }
      System.out.println();
    }
    System.out.println();
  }

  private static int countOccupiedSeats(char[][] map) {
    int result = 0;
    for(int i = 0; i < map.length; i++) {
      for(int j = 0; j < map[0].length; j++) {
        if(map[i][j] == '#')
          result++;
      }
    }

    return result;
  }

  private static int countOccupiedAdj(char[][] map, int r, int c, PuzzlePart part) {
    int[] dirR = new int[]{-1, -1, 0, +1, +1, +1, 0, -1};
    int[] dirC = new int[]{0, +1, +1, +1, 0, -1, -1, -1};

    int result = 0;
    for(int i = 0; i < dirR.length; i++) {
      int nr = dirR[i] + r;
      int nc = dirC[i] + c;

      while(part == PuzzlePart.PART_TWO && nr >= 0 && nr < map.length && nc >= 0 && nc < map[0].length && map[nr][nc] == '.') {
        nr = dirR[i] + nr;
        nc = dirC[i] + nc;
      }

      if(nr >= 0 && nr < map.length && nc >= 0 && nc < map[0].length && map[nr][nc] == '#')
        result++;
    }
    return result;
  }

  private static char[][] round(char[][] map, PuzzlePart part) {
    int rows = map.length, cols = map[0].length;
    char[][] result = new char[rows][cols];

    for(int i = 0; i < rows; i++){
      for(int j = 0; j < cols; j++) {
        int occupied = countOccupiedAdj(map, i, j, part);

        if(map[i][j] == 'L' && occupied == 0) {
          result[i][j] = '#';
        } else if(map[i][j] == '#' && occupied >= 4 && part == PuzzlePart.PART_ONE) {
          result[i][j] = 'L';
        } else if(map[i][j] == '#' && occupied >= 5 && part == PuzzlePart.PART_TWO) {
          result[i][j] = 'L';
        } else {
          result[i][j] = map[i][j];
        }
      }
    }

    return result;
  }

  public static int solve(char[][] map, PuzzlePart part) {
    char[][] newMap = round(map, part);
    while(!equalMaps(newMap, map)) {
      map = newMap;
      newMap = round(map, part);
    }

    return countOccupiedSeats(map);
  }

  public static void main(String[] args) {
    char[][] map = InputUtils.readLinesAs2DMap("day11/input.txt");

    int part1 = solve(map, PuzzlePart.PART_ONE);
    System.out.println("part1: " + part1);

    int part2 = solve(map,PuzzlePart.PART_TWO);
    System.out.println("part1: " + part2);
  }
}