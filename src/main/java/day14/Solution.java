package day14;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import util.InputUtils;
import util.PuzzlePart;

public class Solution {

  public static String processWithMask(long value, String mask, PuzzlePart part) {
    String binaryValue = Long.toBinaryString(value);
    binaryValue = "0".repeat(64 - binaryValue.length()) + binaryValue;
    char[] binaryValueChars = binaryValue.toCharArray();

    for(int i = 0; i < mask.length(); i++) {
      if(mask.charAt(mask.length() - i - 1) == 'X')
        continue;

      if(part == PuzzlePart.PART_TWO && mask.charAt(mask.length() - i - 1) == '1')
        binaryValueChars[binaryValueChars.length - i - 1] = mask.charAt(mask.length() - i - 1);
      else if(part == PuzzlePart.PART_ONE)
        binaryValueChars[binaryValueChars.length - i - 1] = mask.charAt(mask.length() - i - 1);

    }

    return String.valueOf(binaryValueChars);
  }

  public static long solvePart1(List<String> lines) {
    Map<Long, String> memory = new HashMap<>();
    String mask = "";
    for(String l : lines) {
      if(l.startsWith("mask")) {
        mask = l.split("\\s++")[2];
      } else {
        long address = Long.parseLong(l.substring(l.indexOf('[') + 1, l.indexOf(']')));
        long value = Long.parseLong(l.split("\\s++")[2]);
        String processed = processWithMask(value, mask, PuzzlePart.PART_ONE);
        memory.put(address, processed);
      }
    }

    long sum = 0;
    for(Long k : memory.keySet()){
      sum += Long.parseLong(memory.get(k), 2);
    }

    return sum;
  }

  public static void writeToMemory(Map<Long, String> memory, String mask, long address, long value) {
    String binaryMemoryAddress = processWithMask(address, mask, PuzzlePart.PART_TWO);

    int floatingMask = 1;
    int countX = 0;
    for(char c : mask.toCharArray()) {
      if(c == 'X') {
        floatingMask *= 2;
        countX++;
      }
    }

    for(int i = 0; i < floatingMask; i++) {
      String binI = Integer.toBinaryString(i);
      binI = "0".repeat(countX - binI.length()) + binI;
      int currBit = binI.length() - 1;
      char[] currentMemory = binaryMemoryAddress.toCharArray();

      for(int j = 0; j < mask.length(); j++) {
        if(mask.charAt(mask.length() - j - 1) == 'X') {
          currentMemory[currentMemory.length - j - 1] = binI.charAt(currBit);
          currBit--;
        }
      }

      memory.put(Long.parseLong(String.valueOf(currentMemory), 2), Long.toBinaryString(value));
    }
  }

  public static long solvePart2(List<String> lines) {
    Map<Long, String> memory = new HashMap<>();
    String mask = "";
    for(String l : lines) {
      if(l.startsWith("mask")) {
        mask = l.split("\\s++")[2];
      } else {
        long address = Long.parseLong(l.substring(l.indexOf('[') + 1, l.indexOf(']')));
        long value = Long.parseLong(l.split("\\s++")[2]);
        writeToMemory(memory, mask, address, value);
      }
    }

    long sum = 0;
    for(Long k : memory.keySet()){
      sum += Long.parseLong(memory.get(k), 2);
    }

    return sum;
  }

  public static void main(String[] args) {
    List<String> lines = InputUtils.readLines("day14/input.txt");

    long part1 = solvePart1(lines);
    System.out.println("part1: " + part1);

    long part2 = solvePart2(lines);
    System.out.println("part2: " + part2);
  }
}