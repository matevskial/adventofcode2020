package day17;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import util.InputUtils;
import util.PuzzlePart;

public class Solution {
  private static Map<Cube, Boolean> cubeState;

  public static List<Cube> getNeighbours(Cube c, PuzzlePart part) {
    int[] diff = {0, 1, -1};
    List<Cube> result = new ArrayList<>();
    for(int i = 0; i < diff.length; i++) {
      for(int j = 0; j < diff.length; j++) {
        for(int k = 0; k < diff.length; k++) {
          int lastDimension = part == PuzzlePart.PART_TWO ? diff.length : 1;

          for(int l = 0; l < lastDimension; l++) {
            if(i == 0 && j == 0 && k == 0 && l == 0)
              continue;

            result.add(new Cube(c.x + diff[i], c.y + diff[j], c.z + diff[k], c.w + diff[l]));
          }
        }
      }
    }

    return result;
  }

  public static int countActiveNeighbours(Cube c, PuzzlePart part) {
    List<Cube> neigh = getNeighbours(c, part);
    int result = 0;
    for(Cube nc : neigh) {
      if(cubeState.getOrDefault(nc, false))
        result++;
    }

    return result;
  }

  public static void addInactiveNeighbors(PuzzlePart part) {
    List<Cube> toAdd = new ArrayList<>();
    for(Cube c : cubeState.keySet()) {
      if(cubeState.get(c)) {
        List<Cube> neigh = getNeighbours(c, part);
        for(Cube nc: neigh) {
          if(!cubeState.containsKey(nc))
            toAdd.add(nc);
        }
      }
    }
    for(Cube c : toAdd) {
      cubeState.put(c, false);
    }
  }

  public static int solve(PuzzlePart part) {
    for(int i = 0; i < 6; i++) {
      addInactiveNeighbors(part);

      Map<Cube, Boolean> newCubeState = new HashMap<>();
      for(Cube c : cubeState.keySet()) {
        int activeNeighbours = countActiveNeighbours(c, part);

        boolean state = cubeState.get(c);
        if(state && (activeNeighbours < 2 || activeNeighbours > 3))
          state = false;
        else if(!state && activeNeighbours == 3)
          state = true;

        newCubeState.put(c, state);
      }

      for(Cube c : newCubeState.keySet())
        cubeState.put(c, newCubeState.get(c));
    }
    return (int) cubeState.keySet().stream().filter(c -> cubeState.get(c)).count();
  }

  public static void main(String[] args) {
    List<String> lines = InputUtils.readLines("day17/input.txt");
    cubeState = new HashMap<>();
    Map<Cube, Boolean> initialCubeStateCopy = new HashMap<>();
    for(int i = 0; i < lines.size(); i++) {
      for(int j = 0; j < lines.get(i).length(); j++) {
        boolean state = lines.get(i).charAt(j) == '#';
        Cube c = new Cube(i, j, 0, 0);
        cubeState.put(c, state);
        initialCubeStateCopy.put(c, state);
      }
    }

    int part1 = solve(PuzzlePart.PART_ONE);
    System.out.println("part1: " + part1);

    cubeState = initialCubeStateCopy;

    int part2 = solve(PuzzlePart.PART_TWO);
    System.out.println("part2: " + part2);
  }
}

class Cube{
  public int x, y, z, w;

  public Cube(int x, int y, int z,int w){
    this.x = x;
    this.y = y;
    this.z = z;
    this.w = w;
  }

  @Override
  public int hashCode() {
    return Objects.hash(x, y, z, w);
  }

  @Override
  public boolean equals(Object obj) {
    Cube o = (Cube) obj;

    return this.x == o.x && this.y == o.y && this.z == o.z && this.w == o.w;
  }
}