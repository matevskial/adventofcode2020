package day2;

import java.util.ArrayList;
import java.util.List;

import util.InputUtils;
import util.PuzzlePart;

public class Solution {

  public static int solve(List<Password> passwords, PuzzlePart part) {
    int result = 0;
    for(Password p : passwords) {
      if(p.isValid(part))
        result++;
    }

    return result;
  }

  public static void main(String[] args) throws Exception {
    List<String> lines = InputUtils.readLines("day2/input.txt");
    List<Password> passwords = new ArrayList<>();
    for(String entry : lines) {
      passwords.add(new Password(entry));
    }

    int part1 = solve(passwords, PuzzlePart.PART_ONE);
    System.out.println("part1: " + part1);

    int part2 = solve(passwords, PuzzlePart.PART_TWO);
    System.out.println("part2: " + part2);
  }
}

class Password {
  public int n;
  public int m;
  char requiredChar;

  public String password;

  public Password(String entry) throws Exception {
    String[] parts = entry.split(":");
    password = parts[1].trim();

    String[] parsedCriteria = parts[0].split("\\s+");

    n = Integer.parseInt(parsedCriteria[0].substring(0, parsedCriteria[0].indexOf('-')));
    m = Integer.parseInt(parsedCriteria[0].substring(parsedCriteria[0].indexOf('-') + 1));

    requiredChar = parsedCriteria[1].charAt(0);
  }

  public boolean isValid(PuzzlePart part) {
    if(part == PuzzlePart.PART_ONE)
      return isValidPart1();
    else if(part == PuzzlePart.PART_TWO)
      return isValidPart2();
    else
      return false;
  }

  public boolean isValidPart1() {
    int count = 0;
    for(char c : password.toCharArray()) {
      if(requiredChar == c)
        count++;
    }
    return count >= n && count <= m;
  }

  public boolean isValidPart2() {
    if(password.charAt(n - 1) != requiredChar && password.charAt(m - 1) != requiredChar)
      return false;
    else if(password.charAt(n - 1) == requiredChar && password.charAt(m - 1) == requiredChar)
      return false;
    else
      return true;
  }
}