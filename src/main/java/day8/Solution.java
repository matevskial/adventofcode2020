package day8;

import java.util.List;

import util.InputUtils;
import util.GameConsole;

public class Solution {

  public static long solvePart2(List<String> program) {
    for(int i = 0; i < program.size(); i++){
      String instr = program.get(i);

      String newInstr = String.valueOf(instr);
      if(instr.startsWith("nop"))
        newInstr = instr.replace("nop", "jmp");
      else if(instr.startsWith("jmp"))
        newInstr = instr.replace("jmp", "nop");

      // execute program only if we made change of nop or jmp
      if(!instr.equals(newInstr)) {
        program.set(i, newInstr);
        GameConsole c = new GameConsole(program);
        program.set(i, instr);

        if(!c.execute(true))
          return c.getAcc();
      }
    }
    return 0L;
  }

  public static void main(String[] args) {
    List<String> program = InputUtils.readLines("day8/input.txt");
    GameConsole c = new GameConsole(program);
    c.execute(true);
    long part1 = c.getAcc();
    System.out.println("part1: " + part1);

    long part2 = solvePart2(program);
    System.out.println("part2: " + part2);
  }
}
