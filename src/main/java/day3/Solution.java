package day3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import util.InputUtils;

public class Solution {

  public static int solvePart1(char[][] map, int rowSlope, int colSlope) {
    int result = 0;
    int rows = map.length;
    int cols = map[0].length;

    int r = 0, c = 0;

    while(r < rows) {
      result += (map[r][c] == '#') ? 1 : 0;

      r += rowSlope;
      c = (c + colSlope) % cols;
    }
    return result;
  }

  public static long solvePart2(char[][] map) {
    List<int[]> slopes = new ArrayList<>(Arrays.asList(
      new int[]{1, 1},
      new int[]{1, 3},
      new int[]{1, 5},
      new int[]{1, 7},
      new int[]{2, 1}
      ));

    long result = 1;
    for(int[] s : slopes) {
      result *= solvePart1(map, s[0], s[1]);
    }
    return result;
  }

  public static void main(String[] args) {
    char[][] map = InputUtils.readLinesAs2DMap("day3/input.txt");

    int part1 = solvePart1(map, 1, 3);
    System.out.println("part1: " + part1);

    long part2 = solvePart2(map);
    System.out.println("part2: " + part2);
  }
}
