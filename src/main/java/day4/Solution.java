package day4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import util.InputUtils;
import util.PuzzlePart;

public class Solution {

  public static List<Passport> getPassports(List<String> lines) {
    List<Passport> result = new ArrayList<>();

    Passport p = null;
    for(String line : lines) {
      if(line.equals("")) {
        if(p != null) {
          result.add(p);
          p = null;
        }
        continue;
      }

      if(p == null)
        p = new Passport();

      String[] data = line.split("\\s+");
      for(String entry : data) {
        String[] parts = entry.split(":");
        p.setField(parts[0], parts[1]);
      }
    }

    if(p != null)
      result.add(p);

    return result;
  }

  public static int solve(List<Passport> passports, PuzzlePart part) {
    return (int)passports.stream().filter(p -> p.isValid(part)).count();
  }

  public static void main(String[] args) {
    List<String> lines = InputUtils.readLines("day4/input.txt");
    List<Passport> passports = getPassports(lines);

    int part1 = solve(passports, PuzzlePart.PART_ONE);
    System.out.println("part1: " + part1);

    int part2 = solve(passports, PuzzlePart.PART_TWO);
    System.out.println("part2: " + part2);
  }
}

class Passport {
  Map<String, String> fields;
  Set<String> eyeColors;

  public Passport() {
    fields = new HashMap<>();
    eyeColors= Set.of("amb", "blu", "brn", "gry", "grn", "hzl", "oth");
  }

  public void setField(String field, String value) {
    fields.put(field, value);
  }

  public String getField(String field) {
    return fields.getOrDefault(field, "");
  }

  public boolean isValid(PuzzlePart part) {
    boolean validNumberOfFields = fields.size() == 8 || (fields.size() == 7 && !fields.containsKey("cid"));

    if(part == PuzzlePart.PART_ONE)
      return validNumberOfFields;

    boolean result = true;
    for(String field : fields.keySet()) {
      String value = fields.get(field);

      result &= validate(field, value);
    }

    return result & validNumberOfFields;
  }

  private boolean validateNumber(String value, int length, int from, int to) {
    try {
      if(value.length() == length) {
        int num = Integer.parseInt(value);
        return  num >= from && num <= to;
      } else {
        return false;
      }
    } catch (NumberFormatException e) {
      return false;
    }
  }

  private boolean validateNumber(String value, int length, int radix) {
    try {
      if(value.length() == length) {
        Integer.parseInt(value, radix);
        return true;
      } else {
        return false;
      }
    } catch (NumberFormatException e) {
      return false;
    }
  }

  private boolean validateHeight(String value, int length) {
    if(value.endsWith("cm"))
      return validateNumber(value.substring(0, value.length() - 2), 3, 150, 193);
    else if(value.endsWith("in"))
      return validateNumber(value.substring(0, value.length() - 2), 2, 59, 76);
    else
      return false;
  }

  private boolean validateColor(String value) {
    if(value.startsWith("#"))
      return validateNumber(value.substring(1), 6, 16);
    else
      return false;
  }

  private boolean validate(String field, String value) {
    if(field.equals("byr"))
      return validateNumber(value, 4, 1920, 2002);
    else if(field.equals("iyr"))
      return validateNumber(value, 4, 2010, 2020);
    else if(field.equals("eyr"))
      return validateNumber(value, 4, 2020, 2030);
    else if(field.equals("hgt"))
      return validateHeight(value, 6);
    else if(field.equals("hcl"))
      return validateColor(value);
    else if(field.equals("ecl"))
      return eyeColors.contains(value);
    else if(field.equals("pid"))
      return validateNumber(value, 9, 10);
    else
      return true;
  }

  public String toString() {
    return fields.toString();
  }
}