package day9;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import util.InputUtils;

public class Solution {

  private static boolean isValidNumber(Long num, List<Long> preamble) {
    Set<Long> seenComplement = new HashSet<>();
    for(Long i : preamble) {
      Long complement = num - i;
      if(seenComplement.contains(complement))
        return true;

      seenComplement.add(i);
    }
    return false;
  }

  public static Long solvePart1(List<Long> numbers, int preambleSize) {
    List<Long> preamble = new LinkedList<>();
    for(int i = 0; i < preambleSize; i++)
      preamble.add(numbers.get(i));

    for(int i = preambleSize; i < numbers.size(); i++) {
      if(!isValidNumber(numbers.get(i), preamble))
        return numbers.get(i);

      preamble.remove(0);
      preamble.add(numbers.get(i));
    }
    return Long.MIN_VALUE;
  }

  public static Long solvePart2(List<Long> numbers, Long target) {
    for (int i = 0; i < numbers.size(); i++) {
      Long sum = numbers.get(i);
      for (int j = i + 1; j < numbers.size(); j++) {
        sum += numbers.get(j);

        if (sum.equals(target)) {
          Long max = IntStream.range(i, j + 1).mapToObj(numbers::get).max(Long::compareTo).orElse(Long.MIN_VALUE);
          Long min = IntStream.range(i, j + 1).mapToObj(numbers::get).min(Long::compareTo).orElse(Long.MAX_VALUE);
          return max + min;
        }
      }
    }

    return Long.MIN_VALUE;
  }

  public static void main(String[] args) {
    List<Long> numbers = InputUtils.readLines("day9/input.txt")
                                   .stream().map(Long::parseLong).collect(Collectors.toList());

    Long part1 = solvePart1(numbers, 25);
    Long part2 = solvePart2(numbers, part1);
    System.out.println("part1: " + part1);
    System.out.println("part2: " + part2);
  }
}