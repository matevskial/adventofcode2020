package day7;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import util.InputUtils;

public class Solution {

  public static Map<String, List<Baggage>> parseRules(List<String> lines) {
    Map<String, List<Baggage>> result = new HashMap<>();
    for(String line : lines) {
      String[] parts = line.split("\\s+contain\\s+");

      String bag = parts[0];
      if(bag.endsWith("s"))
        bag = bag.substring(0, bag.length() - 1);

      result.put(bag, new ArrayList<>());

      if(parts[1].equals("no other bags."))
        continue;

      String[] baggages = parts[1].split(",\\s+");

      for(String baggage : baggages) {
        String[] parsed = baggage.split("\\s+");
        String b = "";
        for(int i = 1; i < parsed.length; i++)
          b += parsed[i] + " ";

        while(b.endsWith("s") || b.endsWith(".") || b.endsWith(" "))
          b = b.substring(0, b.length() - 1);

        int c = Integer.parseInt(parsed[0]);

        result.get(bag).add(new Baggage(b, c));
      }

    }

    return result;
  }

  public static boolean bfs(Map<String, List<Baggage>> rules, String startBag, String endBag) {
    Queue<String> q = new LinkedList<>();
    Set<String> visited = new HashSet<>();
    q.add(startBag);
    visited.add(startBag);
    while(!q.isEmpty()) {
      String currBag = q.poll();

      if(currBag.equals(endBag))
        return true;

      for(Baggage nextBag : rules.get(currBag)) {
        if(!visited.contains(nextBag.bag)) {
          visited.add(nextBag.bag);
          q.add(nextBag.bag);
        }
      }
    }
    return false;
  }

  public static int solvePart1(Map<String, List<Baggage>> rules, String targetBag) {
    int count = 0;
    for(String bag : rules.keySet()) {
      if(bfs(rules, bag, targetBag))
        count++;
    }
    return count - 1;
  }

  public static int dfs(Map<String, List<Baggage>> rules, String startBag) {
    if(rules.get(startBag).size() == 0)
      return 0;

    int count = 0;
    for(Baggage nextBag : rules.get(startBag))
      count += nextBag.count + nextBag.count * dfs(rules, nextBag.bag);

    return count;
  }

  public static int solvePart2(Map<String, List<Baggage>> rules, String targetBag) {
    int result = 0;
    return dfs(rules, targetBag);
  }

  public static void main(String[] args) {
    List<String> lines = InputUtils.readLines("day7/input.txt");

    Map<String, List<Baggage>> rules = parseRules(lines);

    int part1 = solvePart1(rules, "shiny gold bag");
    System.out.println("part1: " + part1);

    int part2 = solvePart2(rules, "shiny gold bag");
    System.out.println("part2: " + part2);
  }
}

class Baggage {
  public String bag;
  public int count;

  public Baggage(String bag, int count) {
    this.bag = bag;
    this.count = count;
  }
}
