package day10;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import util.InputUtils;

public class Solution {

  public static int solvePart1(List<Integer> adapters) {
    int oneDiff = 0, threeDiff = 1;
    int lastJoltRating = 0;
    for(Integer r : adapters) {
      int diff = r - lastJoltRating;
      if(diff == 1)
        oneDiff++;
      else if(diff == 3)
        threeDiff++;

      lastJoltRating = r;
    }

    return oneDiff * threeDiff;
  }

  public static long solvePart2(List<Integer> adapters) {
    Map<Integer, Long> memo = new HashMap<>();
    memo.put(adapters.get(0), 1L);
    memo.put(0, 1L);
    for(int i = 1; i < adapters.size(); i++) {
      int r = adapters.get(i);
      long result = memo.getOrDefault(r - 1, 0L) +
                   memo.getOrDefault(r - 2, 0L) +
                   memo.getOrDefault(r - 3, 0L);
      memo.put(r, result);
    }

    return memo.get(adapters.get(adapters.size() - 1));
  }

  public static void main(String[] args) {
    List<Integer> adapters = InputUtils.readAsListOfIntegers("day10/input.txt");

    Collections.sort(adapters);
    int part1 = solvePart1(adapters);
    long part2 = solvePart2(adapters);
    System.out.println("part1: " + part1);
    System.out.println("part2: " + part2);
  }
}
