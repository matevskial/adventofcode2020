package day5;

import java.util.ArrayList;
import java.util.List;

import util.InputUtils;

public class Solution {

  public static int convert(String code, char zero, char one) {
    String n = code.replace(zero, '0').replace(one, '1');
    return Integer.parseInt(n, 2);
  }

  public static List<Integer> getSeatIDs(List<String> lines) {
    List<Integer> result = new ArrayList<>();
    for(String line : lines) {
      String rowCode = line.substring(0, 7);
      String colCode = line.substring(7);
      int id = convert(rowCode, 'F', 'B') * 8 + convert(colCode, 'L', 'R');
      result.add(id);
    }

    return result;
  }

  public static int getMissingSeatID(List<Integer> seatIDS) {
    for(int id = 0; id < 1024; id++) {
      if(!seatIDS.contains(id) && seatIDS.contains(id-1) && seatIDS.contains(id+1))
        return id;
    }
    return -1;
  }

  public static void main(String[] args) {
    List<String> lines = InputUtils.readLines("day5/input.txt");
    List<Integer> seatIDs = getSeatIDs(lines);

    int part1 = seatIDs.stream().max(Integer::compare).get();
    System.out.println("part1: " + part1);

    int part2 = getMissingSeatID(seatIDs);
    System.out.println("part2: " + part2);
  }
}
