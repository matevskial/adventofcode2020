package day1;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import util.InputUtils;

public class Solution {

  public static int solvePart1(List<Integer> expenses, int targetSum) {
    Set<Integer> seen = new HashSet<>();

    for(Integer n : expenses) {
      int complement = targetSum - n;
      if(seen.contains(complement))
        return n * complement;

      seen.add(n);
    }

    return 0;
  }

  public static int solvePart2(List<Integer> expenses, int targetSum) {
    Set<Integer> seenComplements = new HashSet<>();
    for(int i = 0; i < expenses.size(); i++) {
      for(int j = i + 1; j < expenses.size(); j++) {
        int n = expenses.get(i);
        int m = expenses.get(j);
        int complement = targetSum - (n + m);

        if(seenComplements.contains(complement))
          return n * m * complement;

        seenComplements.add(n);
        seenComplements.add(m);
      }
    }

    return 0;
  }

  public static void main(String[] args) throws Exception {
    List<String> lines = InputUtils.readLines("day1/input.txt");
    List<Integer> expenseReport = lines
      .stream()
      .map(Integer::valueOf).collect(Collectors.toList());

    int part1 = solvePart1(expenseReport, 2020);
    System.out.println("part1: " + part1);

    int part2 = solvePart2(expenseReport, 2020);
    System.out.println("part2: " + part2);
  }
}
