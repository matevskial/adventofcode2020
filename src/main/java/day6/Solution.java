package day6;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import util.InputUtils;
import util.PuzzlePart;

public class Solution {

  public static int solve(List<String> lines, PuzzlePart part) {
    Map<Character, AtomicInteger> count = new HashMap<>();
    int result = 0;
    int people = 0;
    for(int i = 0; i < lines.size(); i++) {

      if(!lines.get(i).isBlank()) {
        for(char c : lines.get(i).trim().toCharArray()) {
          if(!count.containsKey(c))
            count.put(c, new AtomicInteger(0));

          count.get(c).incrementAndGet();
        }
        people++;
      }


      if(lines.get(i).isBlank() || i == lines.size() - 1) {
        int finalPeople = part == PuzzlePart.PART_ONE ?  0 : people;
        result += (int)count.values().stream().filter(v -> v.get() >= finalPeople).count();

        people = 0;
        count.clear();
      }

    }
    return result;
  }

  public static void main(String[] args) {
    List<String> lines = InputUtils.readLines("day6/input.txt");

    int part1 = solve(lines, PuzzlePart.PART_ONE);
    System.out.println("part1: " + part1);

    int part2 = solve(lines, PuzzlePart.PART_TWO);
    System.out.println("part2: " + part2);
  }
}
